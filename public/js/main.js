(function () {
    site.fn.init = function () {
        site.app = new site.model.App('wss://server.estonsecurity.nl:443/etrace/ws/', '8QQAxnaNUEA0zm7x0HxPIACkm4EePbAg');

        site.options.defaultNotification = {
            'z_index': 10000,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        };
    };

    site.fn.makePopUpInfo = function (patient) {
        var table = $("<table>").addClass("table");
        var thead = $("<thead>");
        var tbody = $("<tbody>");

        thead.append("<tr><th><strong>" + "PatientID:" + "</strong></th><th>" + patient.customId + "</th></tr>");
        thead.append("<tr><th><strong>" + "Last update:" + "</strong></th><th>" + $.timeago(patient.lastUpdate) + "</th></tr>");

        tbody.append("<tr><td><strong>" + "Street:" + "</strong></td><td class=\"street\">Loading...</td></tr>");
        tbody.append("<tr><td><strong>" + "Municipality:" + "</strong></td><td class=\"municipality\">Loading...</td></tr>");
        tbody.append("<tr><td><strong>" + "Province:" + "</strong></td><td class=\"province\">Loading...</td></tr>");
        tbody.append("<tr><td><strong>" + "Country:" + "</strong></td><td class=\"country\">Loading...</td></tr>");

        tomtom.reverseGeocode().position([patient.currentLocation.latitude, patient.currentLocation.longitude]).go(function (geoResponse) {
            table.find(".street").text(geoResponse.address.streetName);
            table.find(".municipality").text(geoResponse.address.municipality);
            table.find(".province").text(geoResponse.address.countrySubdivision);
            table.find(".country").text(geoResponse.address.country);
        });

        var row = $("<tr><th colspan=\"2\" class=\"text-center\"><strong><a href=\"#\">Go to residence</a></strong></th></tr>");
        row.find("a").on("click", function(e) {
            e.preventDefault();
            site.app.map.locateCircle();
        });
        tbody.append(row);

        return table.append(thead).append(tbody)[0];
    };

    $(site.fn.init);
})();