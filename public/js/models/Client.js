site.model.Client = function (url, protocols) {
    this._connected = false;

    this.url = url;
    this.protocols = protocols;
    this.websocket = null;
    this.nextFn = null;
    this.patients = [];

    this.onConnection = null;
    this.onConnectionLost = null;
    this.onAuthenticate = null;
    this.onTotp = null;
    this.loginSuccessHandler = null;
    this.loginErrorHandler = null;
    this.onPatientsLoaded = null;
    this.onPatientUpdate = null;

    this.init();
};

site.model.Client.prototype.init = function () {
    this._connected = false;

    this.websocket = new WebSocket(this.url, this.protocols);
    this.websocket.onopen = $.proxy(this._onOpen, this);
    this.websocket.onclose = $.proxy(this._onClose, this);
    this.websocket.onmessage = $.proxy(this._onMessage, this);
    this.websocket.onerror = $.proxy(this._onError, this);
};

site.model.Client.prototype._call = function (fn) {
    this.nextFn = fn;
    this.nextFn();
};

site.model.Client.prototype._onOpen = function (event) {
    if (typeof this.onConnection === 'function') {
        this.onConnection();
    }

    this._connected = true;
    this._call(this.reloadPatients)
};

site.model.Client.prototype._onClose = function (event) {
    if (this._connected && typeof this.onConnectionLost === 'function') {
        this.onConnectionLost();
    }
    this.init();
};

site.model.Client.prototype._onMessage = function (event) {
    var data = JSON.parse(event.data);

    switch (data.type) {
        case 'authentication_failed':
            if (typeof this.loginErrorHandler === 'function') {
                this.loginErrorHandler();
            }
        case 'authenticate':
            if (typeof this.onAuthenticate === 'function') {
                this.onAuthenticate();
            }
            break;
        case 'totp':
            if (typeof this.onTotp === 'function') {
                this.onTotp();
            }
            break;
        case 'authentication_success':
            if (typeof this.nextFn === 'function') {
                this.nextFn();
            }

            if (typeof this.loginSuccessHandler === 'function') {
                this.loginSuccessHandler();
            }
            break;
        case 'patients':
            this.patients = data.data.patients;

            if (typeof this.onPatientsLoaded === 'function') {
                this.onPatientsLoaded(this.patients);
            }
            break;
        case 'patient':
            var patient = data.data.patient;

            for(var i = 0; i < this.patients.length; i++) {
                if(this.patients[i].id !== patient.id) {
                    continue;
                }

                this.patients[i] = patient;

                if(typeof this.onPatientUpdate === 'function') {
                    this.onPatientUpdate(this.patients[i]);
                }
            }
            break;
    }
};

site.model.Client.prototype._onError = function (event) {
    console.log('error', event);
};

site.model.Client.prototype.sendMessage = function (data) {
    this.websocket.send(JSON.stringify(data))
};

site.model.Client.prototype.login = function (username, password) {
    this.sendMessage({
        type: 'authenticate',
        data: {
            username: username,
            password: password
        }
    });
};

site.model.Client.prototype.totpAuth = function (totp) {
    this.sendMessage({
        type: 'totp',
        data: {
            password: totp
        }
    });
};

site.model.Client.prototype.logout = function () {
    this.sendMessage({
        type: 'logout'
    });
};

site.model.Client.prototype.reloadPatients = function () {
    this.sendMessage({
        type: 'patients'
    });
};