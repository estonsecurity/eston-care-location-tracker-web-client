site.model.Map = function ($el, options) {
    this.map = null;
    this.circle = null;
    this.accuracyCircle = null;
    this.marker = null;
    this.line = null;
    this.patient = null;

    this.init($el, options);
};

site.model.Map.prototype.init = function ($el, options) {
    this.map = tomtom.map($el[0], options);
};

site.model.Map.prototype.drawCircle = function (latitude, longitude, distance, accuracy) {
    var color = '#27ae60';
    var fillColor = '#2ecc71';

    if (accuracy) {
        color = '#2980b9';
        fillColor = '#3498db';

        if (this.accuracyCircle) {
            this.accuracyCircle.remove();
        }
    } else if (this.circle) {
        this.circle.remove();
    }

    var circle = tomtom.L.circle([latitude, longitude], distance, {
        color: color,
        fillColor: fillColor,
        fillOpacity: 0.25
    }).addTo(this.map);

    if (accuracy) {
        this.accuracyCircle = circle;
    } else {
        this.circle = circle;
    }
};

site.model.Map.prototype.drawMarker = function (latitude, longitude) {
    if (this.marker) {
        this.marker.remove();
    }

    this.marker = tomtom.L.marker([latitude, longitude], {
        icon: tomtom.L.svgIcon({
            icon: {
                icon: "fa fa-user",
                iconSize: [32, 37],
                iconAnchor: [16, 4],
                style: {
                    color: '#fff'
                },
                noPlainSVG: true
            }
        })
    }).addTo(this.map);
};

site.model.Map.prototype.drawLine = function (fromLatitude, fromLongitude, toLatitude, toLongitude) {
    if (this.line) {
        this.line.remove();
    }

    this.line = tomtom.L.polyline([
        [fromLatitude, fromLongitude],
        [toLatitude, toLongitude]
    ], {
        color: '#e74c3c'
    }).addTo(this.map);
};

site.model.Map.prototype.locateCircle = function () {
    if (!this.circle) {
        return;
    }

    this.map.setView([this.circle._latlng.lat, this.circle._latlng.lng], 14);
};

site.model.Map.prototype.setPatient = function (patient) {
    this.patient = patient;
    this.reloadPatient();
};

site.model.Map.prototype.reloadPatient = function () {
    if (!this.patient) {
        return;
    }

    this.drawCircle(this.patient.residence.latitude, this.patient.residence.longitude, this.patient.maxDistance);
    this.drawCircle(this.patient.currentLocation.latitude, this.patient.currentLocation.longitude, 20, true);
    this.drawMarker(this.patient.currentLocation.latitude, this.patient.currentLocation.longitude);

    if (this.patient.outOfRange) {
        this.drawLine(this.patient.currentLocation.latitude, this.patient.currentLocation.longitude, this.patient.residence.latitude, this.patient.residence.longitude);
    } else if(this.line) {
        this.line.remove();
        this.line = null;
    }

    this.marker.bindPopup(site.fn.makePopUpInfo(this.patient));
    this.locatePatient();
};

site.model.Map.prototype.locatePatient = function () {
    if (!this.patient) {
        return;
    }

    this.map.setView([this.patient.currentLocation.latitude, this.patient.currentLocation.longitude], 16);
};

site.model.Map.prototype.clear = function () {
    if (this.circle) {
        this.circle.remove();
        this.circle = null;
    }

    if (this.accuracyCircle) {
        this.accuracyCircle.remove();
        this.accuracyCircle = null;
    }

    if (this.marker) {
        this.marker.remove();
        this.marker = null;
    }

    if (this.line) {
        this.line.remove();
        this.line = null;
    }

    this.map.setView([0, 0], 2);
};