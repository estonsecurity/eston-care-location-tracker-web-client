site.model.App = function (serverUrl, mapKey) {
    this.client = null;
    this.map = null;

    this.elements = {};
    this.elements.wrapper = $("#wrapper");
    this.elements.map = $("#map");
    this.elements.header = this.elements.wrapper.find("> header");
    this.elements.connection_status = this.elements.wrapper.find(".connection-status");
    this.elements.login_form = this.elements.header.find("#login").find("form[action=\"authenticate\"]");
    this.elements.totp_form = this.elements.header.find("#totp").find("form[action=\"authenticate_totp\"]");
    this.elements.menu_button = this.elements.header.find("#menu-button");
    this.elements.search_input = this.elements.header.find("#search-patients + input[name=\"search\"]");
    this.elements.patients = $("#patients").find("ul");
    this.elements.important_patients = $("#important-patients").find("ul");

    this.init(serverUrl, mapKey);
};

site.model.App.prototype.init = function (serverUrl, mapKey) {
    this.client = new site.model.Client(serverUrl);
    this.client.onConnection = $.proxy(this.connectionHandler, this);
    this.client.onConnectionLost = $.proxy(this.connectionLostHandler, this);
    this.client.onAuthenticate = $.proxy(this.loginHandler, this);
    this.client.onTotp = $.proxy(this.totpHandler, this);
    this.client.loginSuccessHandler = $.proxy(this.loginSuccessHandler, this);
    this.client.loginErrorHandler = $.proxy(this.loginErrorHandler, this);
    this.client.onPatientsLoaded = $.proxy(this.redrawPatients, this);
    this.client.onPatientUpdate = $.proxy(this.onPatientUpdate, this);

    this.map = new site.model.Map(this.elements.map, {
        key: mapKey
    });

    this.elements.login_form.on("submit", $.proxy(this.onLoginFormSubmitHandler, this));
    this.elements.totp_form.on("submit", $.proxy(this.onTotpFormSubmitHandler, this));

    this.elements.header.find("> footer button.logout")
        .on("click", $.proxy(this.onLogoutButtonClickHandler, this));

    this.elements.menu_button.on("click", $.proxy(this.onMenuClickHandler, this));

    this.elements.search_input.on("keyup", $.proxy(this.onSearch, this));
    this.elements.search_input.find("+ .input-group-btn > button").on("click", $.proxy(this.clearSearch, this));
};

site.model.App.prototype.notify = function (message, type) {
    return $.notify({
        message: message
    }, $.extend(site.options.defaultNotification, {
        type: type
    }));
};

site.model.App.prototype.connectionHandler = function () {
    this.elements.connection_status.removeClass("disconnected").addClass("connected")
        .find(".status-text").text("Connected");
    // this.notify('Connected to the server!', 'success');
    this.loginHandler();
};

site.model.App.prototype.connectionLostHandler = function () {
    this.elements.connection_status.removeClass("connected").addClass("disconnected")
        .find(".status-text").text("Disconnected");
    this.notify('Lost connection to the server!', 'danger');
    this.loginHandler();
};

site.model.App.prototype.checkConnection = function () {
    if (!this.client._connected) {
        this.notify('There is no connection with the location tracker service! Please try again when a connection ' +
            'is established. (See the bottom left corner for the connection status)', 'danger');
    }

    return this.client._connected;
};

site.model.App.prototype.onLoginFormSubmitHandler = function (e) {
    e.preventDefault();

    if (!this.checkConnection()) {
        return;
    }

    var form = $(e.target);

    if (!form.is("form")) {
        form = form.parents("form");
    }

    var usernameInput = form.find("[name=\"username\"]");
    var passwordInput = form.find("[name=\"password\"]");

    var username = usernameInput.val();
    var password = passwordInput.val();

    usernameInput.val("");
    passwordInput.val("");

    this.client.login(username, password);
};

site.model.App.prototype.onTotpFormSubmitHandler = function (e) {
    e.preventDefault();

    if (!this.checkConnection()) {
        return;
    }

    var form = $(e.target);

    if (!form.is("form")) {
        form = form.parents("form");
    }

    var passwordInput = form.find("[name=\"password\"]");

    var password = passwordInput.val();

    passwordInput.val("");

    this.client.totpAuth(password);
};

site.model.App.prototype.loginHandler = function () {
    this.elements.header.addClass("login").removeClass("totp");
    this.elements.login_form.find("[name=\"username\"]").focus();
    this.clearPatients();
    this.clearSearch();
    this.map.clear();
};

site.model.App.prototype.totpHandler = function () {
    this.elements.header.removeClass("login").addClass("totp");
    this.elements.totp_form.find("[name=\"password\"]").focus();
    this.clearPatients();
    this.clearSearch();
    this.map.clear();
};

site.model.App.prototype.loginSuccessHandler = function () {
    this.elements.header.removeClass("login").removeClass("totp");
};

site.model.App.prototype.loginErrorHandler = function () {
    this.loginHandler();
    this.notify("Invalid login credentials, please try again!", "warning");
};

site.model.App.prototype.onLogoutButtonClickHandler = function () {
    this.client.logout();
};

site.model.App.prototype.onSearch = function () {
    var filter = this.elements.search_input.val().toLowerCase();

    this.elements.patients.add(this.elements.important_patients).find("li").each(function (index, value) {
        var item = $(value);

        if (item.text().toLowerCase().indexOf(filter) > -1) {
            item.show();
        } else {
            item.hide();
        }
    });
};

site.model.App.prototype.onMenuClickHandler = function (e) {
    e.preventDefault();
    this.elements.header.toggleClass("closed");
};

site.model.App.prototype.closeMenu = function () {
    this.elements.header.addClass("closed");
};

site.model.App.prototype.clearSearch = function () {
    this.elements.search_input.val("").keyup();
};

site.model.App.prototype.redrawPatients = function (patients) {
    this.elements.patients.html("");
    this.elements.important_patients.html("");

    for (var i = 0; i < patients.length; i++) {
        var patient = patients[i];
        var $patient = $("<li>").text(patient.customId)
            .data("customid", patient.customId).attr("data-customid", patient.customId)
            .on("click", $.proxy(this.onPatientClickHandler, this));

        if (patient.outOfRange) {
            this.elements.important_patients.append($patient);
        } else {
            this.elements.patients.append($patient);
        }
    }
};

site.model.App.prototype.onPatientUpdate = function (patient) {
    if (patient.outOfRange) {
        this.notify("" + patient.customId + " is out of range!", "warning")
            .$ele.find(".message").css("cursor", "pointer").data("customid", patient.customId)
            .on("click", $.proxy(this.onPatientClickHandler, this));
    }

    var patientLists = this.elements.patients.add(this.elements.important_patients);
    var activePatient = this.findPatientByCustomId(
        patientLists.find("li.active").data("customid")
    );

    this.redrawPatients(this.client.patients);

    if (activePatient) {
        var item = patientLists.find("li[data-customid=\"" + activePatient.customId + "\"]");

        if (patient.id === activePatient.id) {
            item.click();
        } else {
            item.addClass("active");
        }
    }
};

site.model.App.prototype.clearPatients = function () {
    this.client.patients = [];
    this.redrawPatients(this.client.patients);
};

site.model.App.prototype.onPatientClickHandler = function (e) {
    e.preventDefault();

    var target = $(e.target);

    if (!target.is("li")) {
        target = target.parents("li");
    }

    this.elements.patients.add(this.elements.important_patients).find("li").removeClass("active");
    target.addClass("active");

    this.map.setPatient(this.findPatientByCustomId(target.data("customid")));
    this.closeMenu();
};

site.model.App.prototype.findPatientByCustomId = function (customId) {
    for (var i = 0; i < this.client.patients.length; i++) {
        var patient = this.client.patients[i];

        if (customId === patient.customId) {
            return patient;
        }
    }

    return null;
};